package models;

public class ModelGenero {

    public class Model {

        public String genero;

        public String getGenero() {
            return genero;
        }

        public void setGenero(String genero) {
            this.genero = genero;
        }

    }
}
