package models;
import java.lang.String;
public class Filme extends Model {

    public String nome;
    public String genero;
    public String dataDeLancamento;
    public String tempoDeDuracao;
    public String gen;

    public Filme(Long id, String nome, String genero, String dataDeLancamento, String tempoDeDuracao, String gen){
        setId(id);
        setNome(nome);
        setdataDeLancamento(dataDeLancamento);
        settempoDeDuracao(tempoDeDuracao);
        ModelGenero gen = new ModelGenero();
        this.genero = genero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public String getdataDeLancamento() {
        return dataDeLancamento;
    }

    public void setdataDeLancamento(String dataDeLancamento) {
        this.dataDeLancamento = dataDeLancamento;
    }

    public String gettempoDeDuracao() {
        return tempoDeDuracao;
    }

    public void settempoDeDuracao(String tempoDeDuracao) {
        this.tempoDeDuracao = tempoDeDuracao;
    }

    @Override
    public String toString() {
        return "Filme{" +
                " Nome='" + nome + '\'' +
                ", Genero='" + genero + '\'' +
                ", Data de lançamento='" + dataDeLancamento + '\'' +
                ", Tempo de duração='" + tempoDeDuracao + '\'' +
                '}';
    }
}


