import Utils.FilmesUtils;
import models.Filme;
import java.lang.String;
import java.util.ArrayList;
import java.util.List;


public class Main {
   public static void main(String[] args) {

      Filme filme = new Filme(new Long(1),  "Um amor para recordar", "Drama, Romance", "24/02/2011", "92/Min");
      Filme filme1 = new Filme(new Long(1),  "It a Coisa", "Terror", "17/08/2017", "112/Min");
      Filme filme2 = new Filme(new Long(1), "Shaft", "Ação, Aventura", " 12/01/2019", "88/Min");
      Filme filme3 = new Filme(new Long(1), "Esquadrao Classe A", "Ação, Suspense", "24/04/2011", "97/Min");
      Filme filme4 = new Filme(new Long(1), "Predadores Assassinos", "Thriller, Ação", "04/09/2019", "95/Min");
      Filme filme5 = new Filme(new Long(1), "Um lugar Sombrio", "Terror, Suspense", "07/03/2017", "101/Min");
      Filme filme6 = new Filme(new Long(1), "Se beber não case!", "Comédia, Aventura", "12/08/2009", " 107/Min");
      Filme filme7 = new Filme(new Long(1), "Plano B", "Comédia, Romance", "28/05/2011", " 98/Min");
      Filme filme8 = new Filme(new Long(1), "Bird Box", "Drama, Suspense", "14/07/2018", "118/Min");
      Filme filme9 = new Filme(new Long(1), "Minios", "Ação, Aventura", "26/06/2015", "110/Min");
      Filme filme10 = new Filme(new Long(1), "A Era do gelo", "Aventura, Infantil", "02/04/2006", "101/Min");

      List<Filme> lista = new ArrayList<>();
      lista.add(filme);
      lista.add(filme1);
      lista.add(filme2);
      lista.add(filme3);
      lista.add(filme4);
      lista.add(filme5);
      lista.add(filme6);
      lista.add(filme7);
      lista.add(filme8);
      lista.add(filme9);
      lista.add(filme10);


      FilmesUtils filmesUtils = new FilmesUtils();
      System.out.println("CATÁLOGO DE FILMES");

      for (Filme meuFilme : lista) {
         filmesUtils.imprime(meuFilme);
      }
      System.out.println("FIM DO CATÁLOGO");


   }
}